﻿using System;
using System.Xml;

using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Xml.Serialization;
using System.IO;

namespace M3APIObjects
{
    /// <summary>
    /// Bulk input object
    /// </summary>
    [Serializable]
    public class M3Bulk
    {
        /// <summary>
        /// Program, example: LstOrders
        /// </summary>
        string program;
        int cono;
        string divi;
        string lanc;
        string locale;
        string timeZone;
        int maxrecs = 0;
        string dateFormat = "YMD8";
        bool excludeEmpty = false;
        bool rightTrim = true;
        string format = "UNFORMATTED";
        bool extendedResult = false;

        public List<Transaction> transactions;

        public M3Bulk()
        {
            this.program = "default";
            this.cono = 0;
            this.transactions = new List<Transaction>();
        }

        public M3Bulk(string program, int cono, int maxrecs = 0)
        {
            this.program = program;
            this.cono = cono;
            this.transactions = new List<Transaction>();
        }

        /// <summary>
        /// Gets all results from the bulk API Call
        /// </summary>
        /// <returns></returns>
        public List<Transaction> GetResults()
        {
            return this.transactions;
        }

        /// <summary>
        /// Returns list of Transactions that has search results
        /// </summary>
        /// <returns></returns>
        public List<Transaction> GetSearchResults()
        {
            List<Transaction> searchResults = new List<Transaction>();

            foreach (Transaction transaction in this.transactions)
            {
                if (transaction.searchResults.Count > 0)
                {
                    searchResults.Add(transaction);
                }
            }
            return searchResults;
        }

        /// <summary>
        /// Clear all search results
        /// </summary>
        public bool ClearSearchResults()
        {
            foreach (Transaction transaction in this.transactions)
            {
                transaction.searchResults = new List<Dictionary<string, string>>();
            }
            return true;
        }

        /// <summary>
        /// Searches results and input
        /// </summary>
        /// <param name="transaction">The transaction to search in (ex: GetData)</param>
        /// <param name="fields">The fields to match, can be input (ex: iCUNO=8273) or output (ex: CUNO=8273) or a comination</param>
        /// <returns></returns>
        public bool SearchAndAdd(string transaction, string fields)
        {
            Dictionary<string, string> inputDict = new Dictionary<string, string>();
            string[] inputFields = fields.Split(';');

            foreach (string fieldAndValue in inputFields)
            {
                inputDict.Add(fieldAndValue.Split('=')[0], fieldAndValue.Split('=')[1]);
            }

            foreach (Transaction t in this.transactions)
            {
                if (t.transaction.Equals(transaction))
                {
                    t.SearchResult(inputDict);
                }
            }
            return true;
        }

        /// <summary>
        /// Add response to input
        /// </summary>
        /// <param name="m3JsonOutput"></param>
        /// <returns></returns>
        public bool AddResponse(string m3JsonOutput)
        {
            JObject jObj = JObject.Parse(m3JsonOutput);
            int outputCounter = 0;
            foreach (JObject result in jObj.SelectToken("results"))
            {
                transactions[outputCounter].AddResponse(result);
                outputCounter++;
            }
            return true;
        }

        public string SearchResultsToXMLString()
        {
            return ToXML(true);
        }

        public string ResultsToXMLString()
        {
            return ToXML();
        }

        public XmlNode SearchResultsToXMLNode()
        {
            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.LoadXml(ToXML(true));
            return xmlDoc.FirstChild;
        }

        public XmlNode ResultsToXMLNode()
        {
            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.LoadXml(ToXML());
            return xmlDoc.FirstChild;
        }

        public MemoryStream SearchResultsToXMLStream()
        {
            MemoryStream memoryStream = new MemoryStream();
            StreamWriter sw = new StreamWriter(memoryStream);
            string xmlString = ToXML(true);
            sw.Write(xmlString);
            sw.Flush();
            memoryStream.Position = 0;
            return memoryStream;
        }

        public MemoryStream ResultsToXMLStream()
        {
            MemoryStream memoryStream = new MemoryStream();
            StreamWriter sw = new StreamWriter(memoryStream);
            string xmlString = ToXML();
            sw.Write(xmlString);
            sw.Flush();
            memoryStream.Position = 0;
            return memoryStream;
        }

        public string ToXML(bool searchResultsOnly = false)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlElement rootElement = xmlDoc.DocumentElement;
            XmlElement programElement = xmlDoc.CreateElement(this.program);

            foreach(Transaction transaction in this.transactions)
            {
                if (!searchResultsOnly)
                {
                    xmlDoc.AppendChild(programElement);
                    XmlElement transElem = xmlDoc.CreateElement(transaction.transaction.Trim());
                    transElem.SetAttribute("status", transaction.status);
                    transElem.SetAttribute("message", transaction.message);
                    foreach (KeyValuePair<string, string> kvp in transaction.inputParameters)
                    {
                        XmlElement inputElem = xmlDoc.CreateElement("i" + kvp.Key);
                        inputElem.InnerText = kvp.Value;
                        transElem.AppendChild(inputElem);
                    }
                    XmlElement recordsElem = xmlDoc.CreateElement("records");

                    foreach (Dictionary<string, string> resultDict in transaction.records)
                    {
                        XmlElement recordElem = xmlDoc.CreateElement("record");
                        foreach (KeyValuePair<string, string> kvp in resultDict)
                        {
                            XmlElement outputElem = xmlDoc.CreateElement(kvp.Key);
                            outputElem.InnerText = kvp.Value;
                            recordElem.AppendChild(outputElem);
                        }
                        recordsElem.AppendChild(recordElem);
                    }
                    transElem.AppendChild(recordsElem);
                    programElement.AppendChild(transElem);
                }
                else
                {
                    if(transaction.searchResults.Count > 0)
                    {
                        xmlDoc.AppendChild(programElement);
                        XmlElement transElem = xmlDoc.CreateElement(transaction.transaction.Trim());
                        transElem.SetAttribute("status", transaction.status);
                        transElem.SetAttribute("message", transaction.message);
                        foreach (KeyValuePair<string, string> kvp in transaction.inputParameters)
                        {
                            XmlElement inputElem = xmlDoc.CreateElement("i" + kvp.Key);
                            inputElem.InnerText = kvp.Value;
                            transElem.AppendChild(inputElem);
                        }
                        XmlElement recordsElem = xmlDoc.CreateElement("records");
                        foreach (Dictionary<string, string> resultDict in transaction.searchResults)
                        {
                            XmlElement recordElem = xmlDoc.CreateElement("record");
                            foreach (KeyValuePair<string, string> kvp in resultDict)
                            {
                                XmlElement outputElem = xmlDoc.CreateElement(kvp.Key);
                                outputElem.InnerText = kvp.Value;
                                recordElem.AppendChild(outputElem);
                            }
                            recordsElem.AppendChild(recordElem);
                        }
                        transElem.AppendChild(recordsElem);
                        programElement.AppendChild(transElem);
                    }
                }
            }
            return xmlDoc.OuterXml;
        }

        /// <summary>
        /// Adds a transaction to the API Call
        /// </summary>
        /// <param name="transaction">The api transaction</param>
        /// <param name="input">Input, example: CONO=100;CUNO=123</param>
        /// <param name="selectedColumns">List of output columns, example: CONO;CUNO;ITNO</param>
        /// <returns></returns>
        public bool AddTransaction(string transaction, string input, string selectedColumns = "")
        {
            Transaction newTransaction = new Transaction(transaction, input, selectedColumns);
            this.transactions.Add(newTransaction);
            return true;
        }

        public bool AddTransaction(string transaction, Dictionary<string, string> input, List<string> selectedColumns)
        {
            Transaction newTransaction = new Transaction(transaction, input, selectedColumns);
            this.transactions.Add(newTransaction);
            return true;
        }

        public string GetURLParameters()
        {
            return "?cono=" + this.cono + "&maxrecs=" + this.maxrecs;
        }

        public string GetBody()
        {
            JObject body = new JObject();
            body.Add("program", this.program);

            JArray transactions = new JArray();
            foreach (Transaction t in this.transactions)
            {
                JObject transaction = new JObject();

                transaction.Add("transaction", t.transaction);

                JObject record = new JObject();
                foreach(KeyValuePair<string, string> kvp in t.inputParameters)
                {
                    record.Add(kvp.Key, kvp.Value);
                }
                transaction.Add("record", record);

                JArray selectedColumns = new JArray();
                if (t.selectedColumns.Count > 0)
                {
                    foreach (string column in t.selectedColumns)
                    {
                        selectedColumns.Add(column);
                    }
                    transaction.Add("selectedColumns", selectedColumns);
                }
                transactions.Add(transaction);
            }
            body.Add("transactions", transactions);

            return body.ToString();
        }
    }

    [Serializable]
    public class Transaction
    {
        public string transaction;
        public string status;
        public string message;
        
        public Dictionary<string, string> inputParameters;
        public List<string> selectedColumns;
        public List<Dictionary<string, string>> records;
        public List<Dictionary<string, string>> searchResults;

        /// <summary>
        /// Searches transaction results for given input/output
        /// </summary>
        /// <param name="inputDict"></param>
        /// <returns></returns>
        public bool SearchResult(Dictionary<string, string> inputDict)
        {
            string value;
            bool hasOutputValues = false;

            foreach(KeyValuePair<string,string> inputKvp in inputDict)
            {
                if (inputKvp.Key.StartsWith("i"))
                {
                    if(inputParameters.TryGetValue(inputKvp.Key.Substring(1), out value))
                    {
                        if (!inputKvp.Value.Equals(value))
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    hasOutputValues = true;
                }
            }

            if (!hasOutputValues)
            {
                this.searchResults = this.records;
                return true;
            }

            foreach (Dictionary<string, string> dict in this.records)
            {
                bool addToSearchResult = true;
                foreach (KeyValuePair<string, string> inputKvp in inputDict)
                {
                    if (!inputKvp.Key.StartsWith("i"))
                    {
                        if (dict.TryGetValue(inputKvp.Key, out value))
                        {
                            if (!inputKvp.Value.Equals(value))
                            {
                                addToSearchResult = false;
                                break;
                            }
                        }
                        else
                        {
                            addToSearchResult = false;
                            break;
                        }
                    }
                }
                if(addToSearchResult)
                    this.searchResults.Add(dict);
            }

            if(this.searchResults.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AddResponse(JObject result)
        {
            this.records = new List<Dictionary<string, string>>();
            Console.WriteLine("In AddResponse");
            Console.WriteLine(result.ToString());

            JToken errorMessage = result.SelectToken("errorMessage");
            if (errorMessage != null)
            {
                this.message = errorMessage.ToString();
                this.status = "NOK";
                Console.WriteLine("ERROR:" + errorMessage);
            }
            else
            {
                this.status = "OK";
                foreach (JObject record in result.SelectToken("records"))
                {
                    Console.WriteLine(record.ToString());
                    Dictionary<string, string> dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(record.ToString());
                    
                    foreach(KeyValuePair<string, string> kvp in dict)
                    {
                        Console.WriteLine("KVP: " + kvp.Key + " V: " + kvp.Value);
                    }
                    this.records.Add(dict);
                }
            }
            return true;
        }

        public Transaction(string transaction, string input, string selectedColumns = "")
        {
            this.inputParameters = new Dictionary<string, string>();
            this.selectedColumns = new List<string>();
            this.searchResults = new List<Dictionary<string, string>>();

            this.transaction = transaction;
            string[] inputArray = input.Split(';');
            foreach (string inputPair in inputArray)
            {
                inputParameters.Add(inputPair.Split('=')[0], inputPair.Split('=')[1]);
            }

            if (!selectedColumns.Trim().Equals(""))
            {
                string[] selectArray = selectedColumns.Split(';');
                foreach (string column in selectArray)
                {
                    this.selectedColumns.Add(column);
                }
            }
        }

        public Transaction(string transaction, Dictionary<string, string> input, List<string> selectedColumns)
        {
            this.inputParameters = new Dictionary<string, string>();
            this.selectedColumns = new List<string>();
            this.transaction = transaction;
            this.inputParameters = input;
            this.selectedColumns = selectedColumns;
            this.status = "";
            this.message = "";
        }
    }

}
